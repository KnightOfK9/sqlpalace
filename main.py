import csv
import facebook
import sys

class User:
    Id = 0
    FirstName = ""
    LastName = ""
    FullName = ""
    Email = ""
    UserType = 0
    FacebookUID = ""

    def print_user(self):
        print("Id :" + str(
            self.Id) + ".First Name :" + self.FirstName + ". LastName :" + self.LastName + ". Full name :" + self.FullName + ". Email:" + self.Email + ". User type:" + str(self.UserType))

    def get_property(self):
        return {"Id": self.Id, "FirstName": self.FirstName, "LastName": self.LastName, "FullName": self.FullName, "Email": self.Email, "UserType": self.UserType}

def set_name(user_dict, user_list, graph, w):
    print("____________________________")
    uid_dict = {}
    for key in sorted(user_dict, key=int):
        value = user_dict[key]
        if value.UserType == "0" and value.FacebookUID != "NULL":
            uid_dict[value.FacebookUID] = value.Id
    chunk_list = chunks(user_list, 50)
    for i in range(0, len(chunk_list) - 1):
        get_and_set_data_list(user_dict, chunk_list[i], uid_dict, graph, w)
        print("Done page " + str(50 * (i + 1)))
    return user_dict

def get_and_set_data_list(user_dict, chunk_list, uid_dict, graph, w):
    uid_list = []
    for i in chunk_list:
        value = user_dict[i]
        if value.UserType == "0" and value.FacebookUID != "NULL":
            uid_list.append(value.FacebookUID)
    data = graph.get_objects(ids=uid_list, fields='id,first_name,last_name,name')
    handle_data(data, user_dict, uid_dict, chunk_list, w)


def handle_data(data, user_dict, uid_dict, chunk_list, w):
    for key, value in data.items():
        u = user_dict[uid_dict[key]]
        u.FirstName = value['first_name']
        u.LastName = value['last_name']
        u.FullName = value['name']
    write_output(user_dict, chunk_list, w)

def write_output(user_dict, chunk_list, w):
    for r in chunk_list:
        value = user_dict[r]
        w.writerow(value.get_property())


def get_key(key):
    try:
        return int(key)
    except ValueError:
        return key

def chunks(l, n):
    n = max(1, n)
    return [l[i:i + n] for i in range(0, len(l), n)]

def main():
    print("Starting")
    if len(sys.argv) > 1:
        access_token = str(sys.argv[2])
        input_file_path = (sys.argv[1])
    else:
        access_token = "EAALsI3A9XyUBAC7v5zQFNFWebITm6xxMZBhlVVhpLmzI14DNFBGgZBKAHBQ29WfdZCMVVNym3LUooCkijFr7aUKZCNDqXNAA0lpSD52QyrhF1AK91wbAL3IRznMdMlQ6UljuVwmItn4ZCUOr8kxupS6nlPa5fbE2vunbGP9st5QZDZD"
        input_file_path = "csv-file/21-6-2016.csv"
    print(input_file_path)
    output_file_path = input_file_path.replace(".csv", "-output.csv")
    graph = facebook.GraphAPI(access_token=access_token, version='2.3')
    with open(input_file_path, encoding="utf8") as user_file:
        with open(output_file_path, 'w', newline='', encoding="utf8")as output_file:
            w = csv.DictWriter(output_file, ["Id", "FirstName", "LastName", "FullName", "Email", "UserType"])
            w.writeheader()
            # user_file = open('csv-file/21-6-2016.csv', encoding="utf8")
            user_reader = csv.reader(user_file)
            user_dict = {}
            user_list = []
            # Extract row and writing to output
            for row in user_reader:

                # Skip first header line
                if user_reader.line_num == 1:
                    continue

                # Skip user role
                if row[5] != "0" and row[5] != "2":
                    print("Skip" + row[5])
                    continue

                user = User()

                user.Id = row[0]
                user.UserType = row[5]
                user.FullName = row[1]
                user_list.append(row[0])
                # Account user
                if user.UserType == "2":
                    user.Email = row[2]
                else:
                    # Facebook user
                    user.Email = row[3]
                    user.FacebookUID = row[4]

                # Print user
                user.print_user()
                user_dict[user.Id] = user
                # Push user to list
            user_list.sort(key=int)
            set_name(user_dict, user_list, graph, w)

main()
exit()